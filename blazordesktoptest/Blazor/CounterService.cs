﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blazordesktoptest.Blazor
{
    public class CounterService
    {
        public delegate void CounterChangedHandler(int counter);
        public event CounterChangedHandler CounterChanged;

        public int Counter
        {
            get;
            private set;
        }

        public void Increment()
        {
            ++Counter;
            CounterChanged?.Invoke(Counter);
        }
    }
}
