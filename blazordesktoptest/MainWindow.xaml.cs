﻿using blazordesktoptest.Blazor;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace blazordesktoptest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        CounterService m_counterService;

        public MainWindow()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddBlazorWebView();
            serviceCollection.AddSingleton<CounterService>();
            ServiceProvider provider = serviceCollection.BuildServiceProvider();
            Resources.Add("services", provider);

            m_counterService = provider.GetService<CounterService>();
            m_counterService.CounterChanged += M_counterService_CounterChanged;

            InitializeComponent();
        }

        private void M_counterService_CounterChanged(int counter)
        {
            lblCounter.Text = "Counter: " + m_counterService.Counter;
        }

        private void btnIncrement_Click(object sender, RoutedEventArgs e)
        {
            m_counterService.Increment();
        }
    }
}
